# V

## Content

```
./V. Smilga:
V. Smilga - Evident nu, inca nelamurit (Teoria relativitatii pe intelesul tuturor).pdf

./Vaclav Havel:
Vaclav Havel - Despre identitatea umana.pdf

./Valentin Muresan:
Valentin Muresan - Ce este filosofia.pdf
Valentin Muresan - Ce vor filosofii.pdf
Valentin Muresan - Comentariu la Etica Nicomahica.pdf
Valentin Muresan - Comentariu la Republica lui Platon.pdf
Valentin Muresan - Despre sensul vietii.pdf
Valentin Muresan - Filosofia morala a lui Richard M. Hare.pdf
Valentin Muresan - Intre Wittgenstein si Heidegger.pdf
Valentin Muresan - Legea morala la Kant.pdf
Valentin Muresan - Trei teorii etice.pdf

./Vasile Frateanu:
Vasile Frateanu - Tratat de metafizica.pdf

./Vasile Musca:
Vasile Musca - Filosofia politica a lui Aristotel.pdf

./Viorel Coltescu:
Viorel Coltescu - Istoria filosofiei vol. 1.pdf
Viorel Coltescu - Istoria filosofiei vol. 2.pdf

./Viorel Vizureanu:
Viorel Vizureanu - Descartes.pdf
Viorel Vizureanu - Proiecte filosofice ale modernitatii V1.pdf

./Virgil Draghici:
Virgil Draghici - Logica traditionala, clasica, modala.pdf

./Vladimir Jankelevitch:
Vladimir Jankelevitch - Curs de filosofie morala.pdf
Vladimir Jankelevitch - Pur si impur.pdf

./Vladimir Lenin:
Vladimir Lenin - Caiete filozofice.pdf
Vladimir Lenin - Materialism si empiriocriticism.pdf

./Vladimir Soloviov:
Vladimir Soloviov - Intreptatirea binelui.pdf
Vladimir Soloviov - Trei dialoguri.pdf

./Voltaire:
Voltaire - Candid sau Optimismul.pdf

./Vulcanescu Mircea:
Vulcanescu Mircea - Nae Ionescu asa cum l-am cunoscut.pdf
```

